FROM rabbitmq:3.7.5-management

RUN rabbitmq-plugins enable --offline rabbitmq_stomp

EXPOSE 61613
